import './ActiveQuiz.css'
import AncwersList from "./AncwersList/AncwersList";

const ActiveQuiz = props => {
    return (
        <div className="ActiveQuiz">
            <p className="Question">
                <span>
                    <strong>
                        {props.answerNumber}.
                    </strong> &nbsp;
                    {props.question}
                </span>
                <small>{props.answerNumber} из {props.quizLenght}</small>
            </p>

            <AncwersList
                state={props.state}
                answers={props.answers}
                onAnswerClick={props.onAnswerClick}
            />
        </div>
    )
}

export default ActiveQuiz;
