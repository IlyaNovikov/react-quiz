import './AncwersList.css'
import AnswerItem from "./AnswerItem/AnswerItem";

const AncwersList = props => {


    return (
        <ul className="AncwersList">
            {props.answers.map((answer, index) => {
                return (
                    <AnswerItem
                        state={props.state ? props.state[answer.id] : null}
                        answer={answer} key={index}
                        onAnswerClick={props.onAnswerClick}
                    />
                )
            })}
        </ul>
    )
}

export default AncwersList
