import axios from "axios";

export default axios.create({
    baseURL: 'https://react-quiz-8d98d-default-rtdb.firebaseio.com/'
})
