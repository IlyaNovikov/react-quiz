import './MenuToggle.css'

const MenuToggle = (props) => {
    const cls = ['MenuToggle']
    if (props.isOpen) {
        cls.push('error');
        cls.push('open')
    } else {
        cls.push('success')
    }
    return (
        <button
            onClick={props.onToggle}
            className={cls.join(' ')}>Меню</button>
    )
}

export default MenuToggle
